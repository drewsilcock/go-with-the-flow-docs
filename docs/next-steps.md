---
title: Next steps
---

# :tada: Next steps

## Congrats!

![well done](/images/well-done.webp){: style="height: 200px; float: right;"}

You've successfully completed this workshop as part of HNCDI Explain's Supercharge Your Cloud Development Workflow - _good job!_

If you want to use any of the code from this tutorial, it's all under an MIT licence so feel free.

Otherwise, I sincerely hope that you've found this tutorial useful, educational and interesting.

## Feedback

Please do stick around to the end of the course so that you can give feedback - it's really important to us!

You can also give (anonymous) feedback on this workshop in particular so that I can improve it in the future:

<iframe
  src="https://docs.google.com/forms/d/e/1FAIpQLSeVAhIlIhekwZHc6rKzo0l7Eu9ZHViXfOtw1LojSsaLo0GZ_Q/viewform?embedded=true"
  width="640"
  height="1378"
  frameborder="0"
  marginheight="0"
  marginwidth="0">
  Loading…
</iframe>
