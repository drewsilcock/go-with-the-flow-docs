# Introduction

## :wave: Welcome

Welcome to the "Go With The Flow" workshop, part of the "Supercharge Your Cloud Development Workflow" course within STFC Hartree Centre's [HNCDI Explain programme](https://www.hartree.stfc.ac.uk/Pages/Explain.aspx){target="_blank" rel="noopener noreferrer"}.

In this workshop, we will be exploring the skills we discussed in the presentation by setting up a git workflow, containerising a small API then using GitLab CI/CD to automatically build and deploy our API to the cloud.

We've got a variety of people from different backgrounds with different experience levels attending this course so it's designed to be taken at your own pace - if you get stuck at any time, we'll be available on the Zoom call to help you out. Just let us know and we can break out into a separate room to chat or discuss it in the main Zoom.

The default branch for the repo is `workshop-start` (instructions on cloning the repo are in the relevant section). There's also a branch called `workshop-completed` which has all the answers in. If you really do get stuck, you can always refer to this for answers (although I'd encourage you to ask in the Zoom first!).

!!! info
    Tempting as it might be, you'll get a lot more out of this tutorial if you go through all the commands and code modifications yourself, rather than just skipping ahead. :slightly_smiling_face:

    I'd also suggest typing out the code and commands youself instead of simply copy & pasting - by typing everything out, you'll do two things:

    1. Better process what you're typing
    2. Probably make typos - this is great! Making mistakes and fixing them is the best way to learn.

Any experience using Go, GitLab CI and AWS would be helpful, but you should be able to going without any prior experience using these.

## :speech_balloon: Giving feedback and getting help

If you get stuck or encounter a problem, the first port of call is to message or chat to us in the Zoom call - we can talk through your problem and see what's going on.

You can also comment on any of these tutorial sections[^disqus] using Disqus - any and all feedback, corrections or questions are very welcome!

[^disqus]: Apart from this intro page :slightly_smiling_face:

## :question: Exercises, hints and answers

Sprinkled among the workshop are a few small exercises for the reader. The hint and answer are hidden by default - you need to click on the coloured bar to show the contents. They look like this:

!!! question "Exercise 5.2"
    Complete this very important file.

??? hint "Hint 5.2 - click to reveal"
    This is a very important hint about how to complete the very important file.

??? answers "Answers 5.2 - click to reveal"
    Alright, here's the completed very important file.
